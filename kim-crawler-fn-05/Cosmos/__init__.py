import datetime
import logging
import requests
import re
from bs4 import BeautifulSoup
from azure.cosmos.cosmos_client import CosmosClient
from azure.cosmos.documents import ConnectionPolicy, ProxyConfiguration
from os import getenv, environ

import azure.functions as func

key=environ['KIM_CRAWLER_DB_ENDPOINT']
auth={'masterKey': environ['KIM_CRAWLER_DB_KEY']}
database_link = 'dbs/fn-05-db'
collection_link = f'{database_link}/colls/url_queue'
connection_policy = ConnectionPolicy()
connection_policy.DisableSSLVerification = True
client = CosmosClient(key, auth, connection_policy=connection_policy)

max_level = int(getenv('KIM_CRAWLER_MAX_LEVEL', 5))


def main(documents: func.DocumentList, content: func.Out[func.Document], err: func.Out[func.Document]) -> str:
    known_urls = set()
    content_docs = func.DocumentList() 
    errors = func.DocumentList() 
    for context in documents:
        url = context['url']
        level = context.get('level', 0)
        response = _get(url, errors)
        response and content_docs.append(_process_content(response.text, context))
        if level < max_level and response:
            _parse_and_fill_in_queue(response.text, context, errors, known_urls)
        else: 
            logging.info(f'max level {level} reached for {url}')
    
    content.set(content_docs)
    err.set(errors)


def _process_content(content: str, context: func.Document) -> func.Document:
    context['content'] = content
    return context


def _parse_and_fill_in_queue(doc: func.Document, context: func.Document, errors: func.DocumentList, known_duplicates: set):
    soup = BeautifulSoup(doc, 'html.parser')
    for link in soup.findAll('a', attrs={'href': re.compile("^http://")}):
        url = link.get('href')
        if url in known_duplicates:
            logging.info(f'known_duplicate: {url}')
            continue

        _create_url_silently(url, context, errors, known_duplicates)


def _create_url_silently(url: str, context: func.Document, errors: func.DocumentList, known_urls: set):
    new_url = _create_url_doc(url, context)
    logging.info(f'creating new url {url}')
    err = None    
    try:
        state = client.CreateItem(collection_link, new_url)
        logging.info(f'item created {new_url}: {state}')
    except Exception as e:
        logging.info(f'failed to create {new_url}: {e}')
    finally:
        known_urls.add(url)

    err and errors.append(func.Document.from_dict(err))


def _create_url_doc(url, context):
    path = context.get('urlPath', [])
    path.append(context['url'])
    level = context.get('level', 0) + 1
    root = context.get('rootUrl', context['url'])
    return {
        'url': url, 
        'rootUrl': root, 
        "timestamp": datetime.datetime.now().isoformat(), 
        'level': level, 
        'urlPath': path
    }


def _get(url: str, errors: func.DocumentList):
    err = None
    try:
        response = requests.get(url)
        if response.status_code != 200:
            err = {'url': url, 'status': response.status_code, 'data': response.text}
    except Exception as e:
        err = {'url': url, 'exception': str(e)}    
    err and errors.append(func.Document.from_dict(err))
    return None if err else response
        