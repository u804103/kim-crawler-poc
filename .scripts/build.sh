#!/bin/bash

SERVICE_NAME=kim-crawler-poc

# Source conda.sh to use conda commands such as conda activate, conda deactivate
. ~/.bashrc
. $(conda info --base)/etc/profile.d/conda.sh

function create {
  remove
  status creating conda env $SERVICE_NAME
  conda env create --file environment.yml --name $SERVICE_NAME
  activate
  pip install --requirement requirements.txt
  pip install --requirement kim-crawler-fn-01/requirements.txt
  pip install --requirement kim-crawler-fn-05/requirements.txt
}

function debug {
  FN=${1:-kim-crawler-fn-01}
  status debugging $FN
  cd $FN
  activate
  func start --language-worker -- "-m ptvsd --host 127.0.0.1 --port 9091"
}

function deploy() {
  FN=${1:-kim-crawler-fn-01}
  status deploying $FN
  cd $FN
  func azure functionapp publish $FN
}

function remove {
  status removing conda env $SERVICE_NAME
  conda deactivate
  conda env remove --name $SERVICE_NAME
}

function activate {
  conda activate $SERVICE_NAME
  status using conda env:
  conda env list | grep '*'
}

function status {
  echo "# $@"
}

"$@"

