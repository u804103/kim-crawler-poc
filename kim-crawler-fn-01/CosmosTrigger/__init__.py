import logging

import azure.functions as func


def main(documents: func.DocumentList, out: func.Out[func.Document]) -> str:
    newdocs = func.DocumentList() 
    for document in documents:
        copy = {'name': document['name']}
        logging.info(f'Document received id: {document["id"]} creating copy: {copy}')
        newdocs.append(func.Document.from_dict(copy))
    
    out.set(newdocs)
