# gitlab setup / clone
edit ~/.gitconfig add proxyconfig
```
[http]
	proxy = http://uXXXXXX:password@dirproxy.mobi.ch:80
```

clone check for user-token if not exists https://cwiki.mobicorp.ch/confluence/display/AFAMM/GitLab+Mobi+Account
```
git clone https://gitlab.com/uXXXXXX/kim-crawler-poc.git
```

# proxy settings
add following lines to your .bashrc
```
alias proxy-auth='export HTTP_PROXY=http://uXXXXXX:$(echo password-base-64-encoded | base64 -d)@dirproxy.mobi.ch:80 && export HTTPS_PROXY=http://uXXXXXX:$(echo password-base-64-encoded | base64 -d)!@dirproxy.mobi.ch:80 && echo proxy http://uXXXXXX:***@dirproxy.mobi.ch:80'
alias proxy='export HTTP_PROXY=http://dirproxy.mobi.ch:80 && export HTTPS_PROXY=http://dirproxy.mobi.ch:80  && echo proxy http://dirproxy.mobi.ch:80'
alias proxy-no='export HTTP_PROXY= && export HTTPS_PROXY= && echo no proxy'
```

# install 

## function tools
```
npm install -g azure-functions-core-tools@3
```

## azure cli
https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest
```
Invoke-WebRequest -Uri https://aka.ms/installazurecliwindows -OutFile .\AzureCLI.msi
```
https://swdportal.mobi.mobicorp.ch/SWDPortalV2/LocalAdminManagement/OrderLocalAdminRights
in explorer wechseln und "AzureCLI.msi > install", Admin-Rechte geben 
init git bash for az `echo "alias az='az.cmd'" >> ~/.bashrc`
```
proxy
az login	
az account set --subscription 2f6df874-de8e-4c0e-890e-11dda7d2c01a
```
# azure resources
* kim-crawler-db: db for deployment stage
...https://portal.azure.com/#@mobiliar.onmicrosoft.com/resource/subscriptions/2f6df874-de8e-4c0e-890e-11dda7d2c01a/resourceGroups/kim-crawler-poc/providers/Microsoft.DocumentDb/databaseAccounts/kim-crawler-db/overview
* kim-crawler-db-local: db for local stage
...https://portal.azure.com/#@mobiliar.onmicrosoft.com/resource/subscriptions/2f6df874-de8e-4c0e-890e-11dda7d2c01a/resourcegroups/kim-crawler-poc/providers/Microsoft.DocumentDB/databaseAccounts/kim-crawler-db-local/overview
* kim-crawler-fn-01
...https://portal.azure.com/#blade/WebsitesExtension/FunctionsIFrameBlade/id/%2Fsubscriptions%2F2f6df874-de8e-4c0e-890e-11dda7d2c01a%2FresourceGroups%2Fkim-crawler-poc%2Fproviders%2FMicrosoft.Web%2Fsites%2Fkim-crawler-fn-01
* kim-crawler-fn-XX
...placeholders for more functions

# run it

## start
install/create
```
./scripts/build.sh create
```
check local
```
proxy-auth
./scripts/build.sh debug [kim-crawler-fn-01]
```
* http-trigger: http://localhost:7071/api/HttpTrigger?name=mobi
* cosmos-db-trigger: 
** go to https://portal.azure.com/#@mobiliar.onmicrosoft.com/resource/subscriptions/2f6df874-de8e-4c0e-890e-11dda7d2c01a/resourcegroups/kim-crawler-poc/providers/Microsoft.DocumentDB/databaseAccounts/kim-crawler-db-local/dataExplorer 
** insert a new document into "fn-db-01/source"
** you should see a message on the log and the same document should have been copied in "fn-db-01/target"

## debug
use visual studio code with python-, azure-functions-plugin and hit f5 
or tell me how it is debugged in intellij

## deploy
deploy to azure
```
proxy
az login	
az account set --subscription 2f6df874-de8e-4c0e-890e-11dda7d2c01a
./scripts/build.sh deploy [kim-crawler-fn-01]
```

# kim-crawler-fn-01
this is a small start with a http- and cosmos-trigger-fn. 
the cosmos-trigger synchronizes a collection 'source' into 'target' in database 'fn-01-db'

## how it was created:
https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-first-azure-function-azure-cli
```
func init kim-crawler-fn-01 --python
cd kim-crawler-fn-01
func new --name HttpTrigger --template "HttpTrigger"
func new --name CosmosTrigger --template "AzureCosmosDBTrigger"
```

# kim-crawler-fn-05
crawling example. cosmos-trigger listens to collection 'url_queue' and rewrites into it.
crawling can be started by creating a document in 'url_queue' in database 'fn-05-db'.
